set qt=C:\Shells\Qt5.5.1\5.5\mingw492_32\

md "..\release\platforms" 1>nul 2>nul
copy /Y "%qt%bin\libstdc++-6.dll" ..\release\
copy /Y "%qt%bin\libgcc_s_dw2-1.dll" ..\release\
copy /Y "%qt%bin\libwinpthread-1.dll" ..\release\
copy /Y "%qt%bin\Qt5Core.dll" ..\release\
copy /Y "%qt%bin\Qt5Gui.dll" ..\release\
copy /Y "%qt%bin\Qt5PrintSupport.dll" ..\release\
copy /Y "%qt%bin\Qt5Widgets.dll" ..\release\
copy /Y "%qt%plugins\platforms\qwindows.dll" ..\release\platforms\
