#ifndef PAINT_H
#define PAINT_H

#include <QWidget>
/*!
@file paint.h
@brief Объявление класса Paint - рисование на widget.
*/
/*
Montgolfier - программа расчета воздушного шара.

Copyright (C) 2010 Igor Ulanov

	Это программа является свободным программным обеспечением. Вы можете распространять и/или модифицировать её согласно условиям Стандартной Общественной Лицензии GNU, опубликованной Фондом Свободного Программного Обеспечения, версии 3 или, по Вашему желанию, любой более поздней версии.
Эта программа распространяется в надежде, что она будет полезной, но БЕЗ ВСЯКИХ ГАРАНТИЙ, в том числе подразумеваемых гарантий ТОВАРНОГО СОСТОЯНИЯ ПРИ ПРОДАЖЕ и ГОДНОСТИ ДЛЯ ОПРЕДЕЛЁННОГО ПРИМЕНЕНИЯ. Смотрите Стандартную Общественную Лицензию GNU для получения дополнительной информации.
Вы должны были получить копию Стандартной Общественной Лицензии GNU вместе с программой. В случае её отсутствия, посмотрите <http://www.gnu.org/licenses/gpl.html>.
*/
/**
Класс рисует на поверхности widget воздушный шар согласно задваемым параметрам. Также считывает из файлов man.jpg или man.bmp или man.tif человечка, которого выводит также на widget для оценки размера будущего шара. Файлы man.jpg, man.bmp и man.tif считываются последовательно в перечисленом порядке, поэтому на widget будет выведен человечек считанный последним. Удобно если в файле будет присутствовать альфа-канал. Если альфа-канала в рисунке нет, то программа создаст его сама, но более качественный результат можно достичь с помощью специализированных инструментов.
*/
class Paint : public QWidget
{
Q_OBJECT
public:
	explicit Paint(QWidget *parent = 0);
private:
	/** Константа со значением роста среднего человека - 175 см. */
	static const float stature=1.75;
protected:
	/** Функция в которой и происходит перерисовка шара и человечка. Функция вызывается системой всякий раз, когда необходимо перерисовать окно программы и принудительно после изменения геометрии или размеров шара. В первой части функции производится определение коэфициентов сжатия для вывода человечка и шара. Во второй части определяются координаты шара на widget'е и отрисовка его с помощью функции QPainter::fillPath. В последней, третьей части - отрисовка человечка.*/
	virtual void paintEvent(QPaintEvent*);
};

#endif // PAINT_H
