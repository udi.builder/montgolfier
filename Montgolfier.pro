# -------------------------------------------------
# Project created by QtCreator 2010-04-14T10:08:44
# -------------------------------------------------
QT += core gui
QT += printsupport
greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = Montgolfier

TEMPLATE = app
SOURCES += main.cpp \
    montgolfier.cpp \
    calculator.cpp \
    machine.cpp \
    paint.cpp \
    print.cpp \
    help.cpp
HEADERS += montgolfier.h \
    calculator.h \
    machine.h \
    paint.h \
    print.h \
    messages.h \
    main.h \
    help.h
FORMS +=
RESOURCES += resource.qrc
RC_FILE = icon.rc

# ======== Translation ===============
TRANSLATIONS = lang/ru_RU.ts
CODECFORTR = UTF-8
