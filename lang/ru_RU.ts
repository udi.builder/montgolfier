<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="ru_RU">
<context>
    <name>HelpBrowser</name>
    <message>
        <source>&amp;Home</source>
        <translation type="vanished">&amp;Главная</translation>
    </message>
    <message>
        <source>&amp;Back</source>
        <translation type="vanished">&amp;Назад</translation>
    </message>
    <message>
        <source>Close</source>
        <translation type="vanished">Закрыть</translation>
    </message>
    <message>
        <source>Esc</source>
        <translation type="vanished">Esc</translation>
    </message>
</context>
<context>
    <name>Montgolfier</name>
    <message>
        <source>Montgolfier</source>
        <translation type="vanished">Монгольфьер</translation>
    </message>
    <message>
        <source>m2</source>
        <translation type="vanished">м2</translation>
    </message>
    <message>
        <source>air weight in a ball:</source>
        <translation type="vanished">вес воздуха в шаре:</translation>
    </message>
    <message>
        <location filename="../montgolfier.cpp" line="390"/>
        <source>diameter of a ball:</source>
        <translation>диаметр шара:</translation>
    </message>
    <message>
        <location filename="../montgolfier.cpp" line="398"/>
        <source>count of points:</source>
        <translation>кол-во отсчетов:</translation>
    </message>
    <message>
        <source>File</source>
        <translation type="vanished">Файл</translation>
    </message>
    <message>
        <source>Language</source>
        <translation type="vanished">Язык</translation>
    </message>
    <message>
        <location filename="../montgolfier.cpp" line="364"/>
        <location filename="../montgolfier.cpp" line="365"/>
        <source>Help</source>
        <translation>Помощь</translation>
    </message>
    <message>
        <source>File recording of the table with a ball pattern</source>
        <translation type="vanished">Запись таблицы с выкройкой в файл</translation>
    </message>
    <message>
        <location filename="../montgolfier.cpp" line="371"/>
        <source>Print</source>
        <translation>Печать</translation>
    </message>
    <message>
        <source>Printing</source>
        <translation type="vanished">Вывод на печать</translation>
    </message>
    <message>
        <source>Printing of the table with a ball pattern</source>
        <translation type="vanished">Распечатка таблицы с выкройкой</translation>
    </message>
    <message>
        <source>End of work of the program</source>
        <translation type="vanished">Завершение работы программы</translation>
    </message>
    <message>
        <source>Save as text</source>
        <translation type="vanished">Запись текстового файла</translation>
    </message>
    <message>
        <location filename="../montgolfier.cpp" line="358"/>
        <location filename="../montgolfier.cpp" line="359"/>
        <source>Save as format CSV</source>
        <translation>Запись файла в формате CSV</translation>
    </message>
    <message>
        <location filename="../montgolfier.cpp" line="361"/>
        <location filename="../montgolfier.cpp" line="362"/>
        <source>Save as format PDF</source>
        <translation>Запись файла в формате PDF</translation>
    </message>
    <message>
        <source>About solar balloons</source>
        <translation type="vanished">О солнечных аэростатах</translation>
    </message>
    <message>
        <source>Program instructions</source>
        <translation type="vanished">Описание программы</translation>
    </message>
    <message>
        <location filename="../montgolfier.cpp" line="377"/>
        <location filename="../montgolfier.cpp" line="378"/>
        <source>License</source>
        <translation>Лицензия</translation>
    </message>
    <message>
        <source>About ...</source>
        <translation type="vanished">Сведения ...</translation>
    </message>
    <message>
        <source>air temperature &lt;------------</source>
        <translation type="vanished">температура воздуха &lt;------------</translation>
    </message>
    <message>
        <source>difference of temperatures ------------&gt;</source>
        <translation type="vanished">разница температур ------------&gt;</translation>
    </message>
    <message>
        <location filename="../montgolfier.cpp" line="397"/>
        <source>ball gores:</source>
        <translation>клиньев:</translation>
    </message>
    <message>
        <source>Save As...</source>
        <translation type="vanished">Записать как ...</translation>
    </message>
    <message>
        <location filename="../montgolfier.cpp" line="374"/>
        <source>Exit</source>
        <translation>Выход</translation>
    </message>
    <message>
        <source>length of a pattern:</source>
        <translation type="vanished">длина выкройки:</translation>
    </message>
    <message>
        <location filename="../montgolfier.cpp" line="392"/>
        <source>pyramid corner:</source>
        <translation>угол раскрыва:</translation>
    </message>
    <message>
        <source>width of a pattern:</source>
        <translation type="vanished">ширина выкройки:</translation>
    </message>
    <message>
        <location filename="../montgolfier.cpp" line="393"/>
        <source>diameter of an exit:</source>
        <translation>диаметр выхода:</translation>
    </message>
    <message>
        <source>the cover area:</source>
        <translation type="vanished">площадь оболочки:</translation>
    </message>
    <message>
        <source>sphere volume:</source>
        <translation type="vanished">объем шара:</translation>
    </message>
    <message>
        <source>material density:</source>
        <translation type="vanished">плотность материала:</translation>
    </message>
    <message>
        <location filename="../montgolfier.cpp" line="385"/>
        <source>cover weight:</source>
        <translation>вес оболочки шара:</translation>
    </message>
    <message>
        <source>carrying power:</source>
        <translation type="vanished">подъемная сила:</translation>
    </message>
    <message>
        <source>step of points:</source>
        <translation type="vanished">шаг отсчетов:</translation>
    </message>
    <message>
        <location filename="../messages.h" line="30"/>
        <source>File record</source>
        <translation>Запись файла</translation>
    </message>
    <message>
        <location filename="../montgolfier.cpp" line="391"/>
        <location filename="../montgolfier.cpp" line="394"/>
        <source>cm</source>
        <translation>см</translation>
    </message>
    <message>
        <source>m3</source>
        <translation type="vanished">м3</translation>
    </message>
    <message>
        <source>g/m2</source>
        <translation type="vanished">г/м2</translation>
    </message>
    <message>
        <source>kg</source>
        <translation type="vanished">кг</translation>
    </message>
    <message>
        <location filename="../montgolfier.cpp" line="22"/>
        <source>English</source>
        <translation>Русский</translation>
    </message>
    <message>
        <location filename="../montgolfier.cpp" line="383"/>
        <source>the cover area (m&lt;sup&gt;2&lt;/sup&gt;):</source>
        <translation>площадь оболочки (м&lt;sup&gt;2&lt;/sup&gt;):</translation>
    </message>
    <message>
        <location filename="../montgolfier.cpp" line="384"/>
        <source>sphere volume (m&lt;sup&gt;3&lt;/sup&gt;):</source>
        <translation>объем шара (м&lt;sup&gt;3&lt;/sup&gt;):</translation>
    </message>
    <message>
        <location filename="../montgolfier.cpp" line="388"/>
        <source>air temperature%1&lt;&lt;&lt;</source>
        <translation>температура%1воздуха%1&lt;&lt;&lt;</translation>
    </message>
    <message>
        <location filename="../montgolfier.cpp" line="389"/>
        <source>&gt;&gt;&gt;%1difference of%1temperatures</source>
        <translation>&gt;&gt;&gt;%1разница%1температур</translation>
    </message>
    <message>
        <source>Save as format text</source>
        <translation type="vanished">Запись текстового файла</translation>
    </message>
    <message>
        <location filename="../montgolfier.cpp" line="287"/>
        <location filename="../montgolfier.cpp" line="300"/>
        <source>Switching on %1 language</source>
        <translation>Переключение на %1 язык</translation>
    </message>
    <message>
        <source>Documents</source>
        <translation type="vanished">Документы</translation>
    </message>
    <message>
        <source>My Documents</source>
        <translation type="vanished">Мои документы</translation>
    </message>
    <message>
        <location filename="../montgolfier.cpp" line="28"/>
        <source>Ready</source>
        <translation>Готово</translation>
    </message>
    <message>
        <location filename="../montgolfier.cpp" line="381"/>
        <source>length of a pattern (cm):</source>
        <translation>длина выкройки (см):</translation>
    </message>
    <message>
        <location filename="../montgolfier.cpp" line="382"/>
        <source>width of a pattern (cm):</source>
        <translation>ширина выкройки (см):</translation>
    </message>
    <message>
        <source>the cover area (m&lt;sup&gt;2&lt;/sup&gt;:</source>
        <translation type="vanished">площадь оболочки (м&lt;sup&gt;2&lt;/sup&gt;):</translation>
    </message>
    <message>
        <source>sphere volume (m&lt;sup&gt;3&lt;/sup&gt;:</source>
        <translation type="vanished">объем шара (м&lt;sup&gt;3&lt;/sup&gt;):</translation>
    </message>
    <message>
        <location filename="../montgolfier.cpp" line="386"/>
        <source>air weight in a ball (kg):</source>
        <translation>вес воздуха в шаре (кг):</translation>
    </message>
    <message>
        <location filename="../montgolfier.cpp" line="387"/>
        <source>carrying power (kg):</source>
        <translation>подъемная сила (кг):</translation>
    </message>
    <message>
        <source>air temperature
&lt;&lt;&lt;</source>
        <translation type="vanished">температура воздуха\n&lt;&lt;&lt;</translation>
    </message>
    <message>
        <source>&gt;&gt;&gt;
difference of
temperatures</source>
        <translation type="vanished">&gt;&gt;&gt;\nразница температур</translation>
    </message>
    <message>
        <location filename="../montgolfier.cpp" line="395"/>
        <source>material density (g/m&lt;sup&gt;2&lt;/sup&gt;):</source>
        <translation>плотность материала (г/м&lt;sup&gt;2&lt;/sup&gt;):</translation>
    </message>
    <message>
        <location filename="../montgolfier.cpp" line="396"/>
        <source>step of points (cm):</source>
        <translation>шаг отсчетов (см):</translation>
    </message>
    <message>
        <source>Text</source>
        <translation type="vanished">Текст</translation>
    </message>
    <message>
        <source>CSV</source>
        <translation type="vanished">CSV</translation>
    </message>
    <message>
        <source>Save as csv</source>
        <translation type="vanished">Запись файла в формате &quot;csv&quot;</translation>
    </message>
    <message>
        <location filename="../montgolfier.cpp" line="367"/>
        <source>&amp;Save as ...</source>
        <translation>&amp;Записать как ...</translation>
    </message>
    <message>
        <location filename="../montgolfier.cpp" line="368"/>
        <source>Save as ...</source>
        <translation>Записать как ...</translation>
    </message>
    <message>
        <location filename="../montgolfier.cpp" line="370"/>
        <source>&amp;Print</source>
        <translation>&amp;Печать</translation>
    </message>
    <message>
        <location filename="../montgolfier.cpp" line="373"/>
        <source>&amp;Exit</source>
        <translation>&amp;Выход</translation>
    </message>
    <message>
        <location filename="../montgolfier.cpp" line="401"/>
        <source>&amp;File</source>
        <translation>&amp;Файл</translation>
    </message>
    <message>
        <location filename="../montgolfier.cpp" line="289"/>
        <location filename="../montgolfier.cpp" line="400"/>
        <source>&amp;Language</source>
        <translation>&amp;Язык</translation>
    </message>
    <message>
        <location filename="../montgolfier.cpp" line="356"/>
        <source>Save as format TXT</source>
        <translation>Запись файла в формате ТХТ</translation>
    </message>
    <message>
        <location filename="../montgolfier.cpp" line="375"/>
        <location filename="../montgolfier.cpp" line="376"/>
        <source>Description</source>
        <translation>Описание</translation>
    </message>
    <message>
        <location filename="../montgolfier.cpp" line="379"/>
        <location filename="../montgolfier.cpp" line="380"/>
        <source>About</source>
        <translation>О программе</translation>
    </message>
    <message>
        <location filename="../montgolfier.cpp" line="399"/>
        <source>&amp;Help</source>
        <translation>&amp;Помощь</translation>
    </message>
    <message>
        <location filename="../montgolfier.cpp" line="454"/>
        <location filename="../montgolfier.cpp" line="455"/>
        <source>%1 cm</source>
        <translation>%1 см</translation>
    </message>
    <message>
        <location filename="../montgolfier.cpp" line="456"/>
        <source>%1 m&lt;sup&gt;2&lt;/sup&gt;</source>
        <translation>%1 м&lt;sup&gt;2&lt;/sup&gt;</translation>
    </message>
    <message>
        <location filename="../montgolfier.cpp" line="457"/>
        <source>%1 m&lt;sup&gt;3&lt;/sup&gt;</source>
        <translation>%1 м&lt;sup&gt;3&lt;/sup&gt;</translation>
    </message>
    <message>
        <location filename="../montgolfier.cpp" line="465"/>
        <location filename="../montgolfier.cpp" line="466"/>
        <location filename="../montgolfier.cpp" line="467"/>
        <source>%1 kg</source>
        <translation>%1 кг</translation>
    </message>
    <message>
        <location filename="../montgolfier.cpp" line="494"/>
        <source>File printing</source>
        <translation>Послано на печать</translation>
    </message>
    <message>
        <location filename="../montgolfier.cpp" line="538"/>
        <location filename="../montgolfier.cpp" line="556"/>
        <location filename="../montgolfier.cpp" line="566"/>
        <location filename="../montgolfier.cpp" line="576"/>
        <source>File recording</source>
        <oldsource>File recording: </oldsource>
        <translation>Записан файл</translation>
    </message>
</context>
<context>
    <name>Print</name>
    <message>
        <location filename="../messages.h" line="20"/>
        <source>diameter of a sphere:</source>
        <translation>диаметр шара:</translation>
    </message>
    <message>
        <location filename="../messages.h" line="21"/>
        <source>length of a pattern:</source>
        <translation>длина выкройки:</translation>
    </message>
    <message>
        <location filename="../messages.h" line="22"/>
        <source>pyramid corner:</source>
        <translation>угол раскрыва:</translation>
    </message>
    <message>
        <location filename="../messages.h" line="23"/>
        <source>width of a pattern:</source>
        <translation>ширина выкройки:</translation>
    </message>
    <message>
        <location filename="../messages.h" line="24"/>
        <source>diameter of an exit:</source>
        <translation>диаметр выхода:</translation>
    </message>
    <message>
        <location filename="../messages.h" line="25"/>
        <source>the cover area:</source>
        <translation>площадь оболочки:</translation>
    </message>
    <message>
        <location filename="../messages.h" line="26"/>
        <source>sphere volume:</source>
        <translation>объем шара:</translation>
    </message>
    <message>
        <location filename="../messages.h" line="27"/>
        <source>material density:</source>
        <translation>плотность материала:</translation>
    </message>
    <message>
        <location filename="../messages.h" line="28"/>
        <source>air weight in a sphere:</source>
        <translation>вес воздуха в шаре:</translation>
    </message>
    <message>
        <location filename="../messages.h" line="29"/>
        <source>cover weight:</source>
        <translation>вес оболочки шара:</translation>
    </message>
    <message>
        <location filename="../messages.h" line="31"/>
        <source>SOLLAR BALL</source>
        <translation>ВОЗДУШНЫЙ ШАР</translation>
    </message>
    <message>
        <location filename="../messages.h" line="35"/>
        <location filename="../messages.h" line="36"/>
        <location filename="../messages.h" line="38"/>
        <location filename="../messages.h" line="39"/>
        <source> cm</source>
        <oldsource>cm</oldsource>
        <translation>см</translation>
    </message>
    <message>
        <location filename="../messages.h" line="40"/>
        <source> m&lt;sup&gt;2&lt;/sup&gt;</source>
        <translation>м&lt;sup&gt;2&lt;/sup&gt;</translation>
    </message>
    <message>
        <location filename="../messages.h" line="41"/>
        <source> m&lt;sup&gt;3&lt;/sup&gt;</source>
        <translation>м&lt;sup&gt;3&lt;/sup&gt;</translation>
    </message>
    <message>
        <location filename="../messages.h" line="42"/>
        <source> g/m&lt;sup&gt;2&lt;/sup&gt;</source>
        <translation>г/м&lt;sup&gt;2&lt;/sup&gt;</translation>
    </message>
    <message>
        <source> m2</source>
        <translation type="vanished">м2</translation>
    </message>
    <message>
        <source> m3</source>
        <oldsource>m2</oldsource>
        <translation type="vanished">м3</translation>
    </message>
    <message>
        <source> g/m2</source>
        <translation type="vanished">г/м2</translation>
    </message>
    <message>
        <location filename="../messages.h" line="43"/>
        <location filename="../messages.h" line="44"/>
        <source> kg</source>
        <translation>кг</translation>
    </message>
</context>
</TS>
