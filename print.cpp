#include "print.h"
#include "calculator.h"
#include "machine.h"
#include "math.h"
#include "messages.h"
#include <QFile>
#include <QTextStream>
#include <QTextDocument>

extern Machine machine;
extern Common *common;
extern Up up;
extern Down down;
extern Pyr pyr;
extern Out out;

Print::Print()
{
}

void Print::Draw(QPrinter* p,int d)
{
	data.reserve(1500); // резервируем место под массив
	Pattern(d); // заполняем массив
	QPainter paint(p);
	paint.setRenderHint(QPainter::Antialiasing, true);
	Ball(&paint);
	paint.setPen(QPen(Qt::black,0.5));
	Head(&paint);
	Table(&paint);
	data.clear(); // удаление массива
}

void Print::Text(int d,QString name,QString s)
{
	data.reserve(1500); // резервируем место под массив
	Pattern(d); // заполняем массив
	QFile file(name); // создаем файл
	if (file.open(QIODevice::WriteOnly | QIODevice::Text))
	{
		QTextStream out(&file);
		out.string();
		out.setNumberFlags(QTextStream::ForcePoint);
		out.setRealNumberNotation(QTextStream::FixedNotation);
		out.setRealNumberPrecision (1);
		out.setFieldAlignment(QTextStream::AlignRight);
		unsigned int n=0;
		QString str;
		if (s=="csv")
		{
			// csv
			out<<"X"<<";"<<"Y/2"<<";"<<"Y"<<"\n"; // первая строка
			// распечатываем массив
			while (n<data.size())
			{
				str.setNum((floor(data[n++]*10+0.5)/10),'F',1);
				out<<str.replace(".",",")<<";";
				str.setNum((floor(data[n++]*10+0.5)/10),'F',1);
				out<<str.replace(".",",")<<";";
				str.setNum((floor(data[n++]*10+0.5)/10),'F',1);
				out<<str.replace(".",",")<<"\n";
			}
		}
		else
		{
			// txt
			out<<"       X     Y/2       Y\n"; // первая строка
			// распечатываем массив
			while (n<data.size())
			{
				out<<QString("%1%2%3\n")
						.arg((floor(data[n]*10+0.5)/10),8,'F',1)
						.arg((floor(data[n+1]*10+0.5)/10),8,'F',1)
						.arg((floor(data[n+2]*10+0.5)/10),8,'F',1);
			n+=3;
			}
		}

		file.close();
	}
	data.clear(); // удаление массива
}

void Print::Table(QPainter* g)
{ // печать таблицы
	QRect pRect;
	y+=g->fontMetrics().height();
	int div=(int)ceil((float)data.size()/9); // количество строк
	int col=(int)((float)g->viewport().width()/10); // размер столбца
	int x=0;
	// вертикальные линии
	for (int i=0;i<3;i++)
	{ // три столбца
		for (int z=0;z<4;z++)
		{ // четыре линии в каждом столбце
			g->drawLine(x,y,x,y+(div+1)*g->fontMetrics().height());
			x+=col;
		}
		x=x-col+col/2;
	}
	// верхняя строчка
	x=0;
	for (int z=0;z<3;z++)
	{ // цикл построения трех столбцов одной строки
		g->drawLine(x,y,x+col*3,y);
		pRect = QRect(x,y,col,g->fontMetrics().height());
		g->drawText(pRect,Qt::AlignCenter,"X");
		x+=col;
		pRect = QRect(x,y,col,g->fontMetrics().height());
		g->drawText(pRect,Qt::AlignCenter,"Y/2");
		x+=col;
		pRect = QRect(x,y,col,g->fontMetrics().height());
		g->drawText(pRect,Qt::AlignCenter,"Y");
		x+=col+col/2;
	}
	y+=g->fontMetrics().height();
	int n=0; // адрес в массиве
	for (int i=0;i<div;i++)
	{ // цикл построения всех строк таблицы
		x=0;
		for (int z=0;z<3;z++)
		{ // цикл построения трех столбцов одной строки
			g->drawLine(x,y,x+col*3,y);
			n=z*div+i;
			if (n*3<(int)data.size())
			{ // если данные в массиве не закончились - выводим данные
				for (int a=0;a<3;a++)
				{ // цикл построения одного столбца
					pRect = QRect(x,y,col,g->fontMetrics().height());
					g->drawText(pRect,Qt::AlignRight,QString("%1 ")
								.arg(floor(data[n*3+a]*10+0.5)/10,0,'f',1));
					x+=col;
				}
			}
			else x+=col; // если закончились, то данные не выводим
			x+=col/2;
		}
		y+=g->fontMetrics().height();
	}
	// нижняя строка
	x=0;
	for (int z=0;z<3;z++)
	{ // цикл построения трех столбцов одной строки
		g->drawLine(x,y,x+col*3,y);
		x+=col/2+col*3;
	}
}

void Print::Pattern(int step)
{ // функция заполнения массива вычисляемыми координатами выкройки
	// расчитываем координаты для шара
	float Step_A=step/(common->r*100); // угол приращения
	float a=0.0;
	int n=0;
	float x=0.0;
	// сначала для верхней части шара ...
	while (a<(pi/2))
		{
		data.push_back(x);
		data.push_back(common->w*sin(a)*50);
		data.push_back(data[data.size()-1]*2);
		x+=step;
		a+=Step_A;
		n+=1;
		}
	// ... теперь для нижней части шара ...
	a=(pi/2)-(a-(pi/2));
	while (a>((pi/2)-common->angle))
		{
		data.push_back(x);
		data.push_back(common->w*sin(a)*50);
		data.push_back(data[data.size()-1]*2);
		x+=step;
		a-=Step_A;
		n+=1;
		}
	// .. теперь для пирамиды
	a=(pyr.Get_L()*100)-(x-(up.Get_L()+down.Get_L())*100);
	Step_A=(common->r_down*pi*2)/(common->n*pyr.Get_L()); // удвоенный тангенс угла
	while (a>(out.Get_L()*100))
		{
		data.push_back(x);
		data.push_back(Step_A*a/2);
		data.push_back(data[data.size()-1]*2);
		x+=step;
		a-=step;
		n+=1;
		}
	data.push_back(machine.Get_L()*100);
	data.push_back(Step_A*out.Get_L()*50);
	data.push_back(data[data.size()-1]*2);
}

void Print::Head(QPainter* g)
{ // функция печати заголовка
	QRect pRect;
	QFont myFont;
	myFont.setFamily("Serif");
	myFont.setBold(true);
	myFont.setPointSize(14);
	g->setFont(myFont);
	pRect = QRect(0,0,g->viewport().width(),g->fontMetrics().height());
	g->drawText(pRect,Qt::AlignCenter,tr(Messages::name[11]));
	y=g->fontMetrics().height()*2;
	myFont.setBold(false);
	myFont.setPointSize(10);
	g->setFont(myFont);
	// вывод параметров шара
	int width=g->viewport().width()/2;
	int w_number=g->fontMetrics().width("000000,0");
	pRect = QRect(0,y,width-w_number,g->fontMetrics().height());
	int x_2=width-w_number-w_number;
	int x_1=x_2-w_number;
	float data[10];
	data[0]=common->r*200;
	data[1]=machine.Get_L()*100;
	data[2]=common->degree;
	data[3]=common->w*100;
	data[4]=common->r_out*200;
	data[5]=machine.Get_S();
	data[6]=machine.Get_V();
	data[7]=common->m;
	data[8]=machine.Get_M_air();
	data[9]=machine.Get_M();
    QTextDocument td;
    td.setDefaultFont(myFont);
    for (int i=0;i<5;i++)
	{
		// левая половина строки
        g->translate(QPointF(0,y));
        td.setHtml(tr(Messages::name[i*2]));
        td.drawContents(g);
        g->translate(QPointF(0,-y));
        td.setHtml(QString::number(ceil(data[i*2]*1000)/1000,'g',4));
        g->translate(QPointF(x_1,y));
        td.drawContents(g);
        g->translate(QPointF(-x_1,-y));
        td.setHtml(tr(Messages::size[i*2]));
        g->translate(QPointF(x_2,y));
        td.drawContents(g);
        g->translate(QPointF(-x_2,-y));
        // правая половина строки
        g->translate(QPointF(width,y));
        td.setHtml(tr(Messages::name[i*2+1]));
        td.drawContents(g);
        g->translate(QPointF(-width,-y));
        td.setHtml(QString::number(ceil(data[i*2+1]*1000)/1000,'g',4));
        g->translate(QPointF(x_1+width,y));
        td.drawContents(g);
        g->translate(QPointF(-(x_1+width),-y));
        td.setHtml(tr(Messages::size[i*2+1]));
        g->translate(QPointF(x_2+width,y));
        td.drawContents(g);
        g->translate(QPointF(-(x_2+width),-y));
        y=y+g->fontMetrics().height();
	}
}

void Print::Ball(QPainter* g)
{
	const float w=20.0; // толщина пера
	float r=common->r; // радиус шара
	float h_ball=r+r*common->sin_A; // высота шаровой части
	float h_pyr=common->cos_A*(pyr.Get_L()-out.Get_L()); // высота пирамиды
	float k=(h_ball+h_pyr)/(g->viewport().height()-w); // шар вписываем в y пикселей
	// если диаметр шара больше ширины области, то вписываем в ширину
	if (((r*2)/k)>(g->viewport().width()-(w*2))) k=(r*2)/(g->viewport().width()-(w*2));
	r=r/k; // радиус шара на рисунке
	h_ball=h_ball/k; // высота шаровой части
	h_pyr=h_pyr/k; // высота пирамиды
	float r_out=common->r_out/k; // радиус выхода
	float bias_y=w/2;
	float bias_ball=(g->viewport().width()-bias_y)/2;
	QPainterPath myPath;
	float Y=bias_y+h_ball+h_pyr;
	float X=bias_ball+r_out;
	myPath.moveTo(X,Y); // правая точка выхода
	myPath.arcTo(bias_ball-r,bias_y,r+r,r+r,(0-common->degree/2),(common->degree+180));
	X=bias_ball-r_out;
	myPath.lineTo(X,Y); // левая точка выхода
	X=bias_ball+r_out;
	myPath.lineTo(X,Y); // правая точка выхода
	g->setRenderHint(QPainter::Antialiasing, true);
	g->setPen(QPen(Qt::lightGray,w));
	g->drawPath(myPath);

}

