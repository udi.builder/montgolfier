#include "calculator.h"
#include <QtMath>
#include <QDebug>

extern Common *common;
extern Up up;
extern Down down;
extern Pyr pyr;
extern Out out;

double Up::Calc_L(void)
{ // функция подсчета длины выкройки
L=(pi*common->r)/2;
return L;
}

double Up::Calc_S(void)
{ // функция подсчета площади выкройки
S=2*pi*common->r*common->r;
return S;
}

double Up::Calc_V(void)
{ // функция подсчета объема выкройки
V=(2*pi*common->r*common->r*common->r)/3;
return V;
}

double Down::Calc_L(void)
{ // функция подсчета длины выкройки
L=common->r*common->angle;
return L;
}

double Down::Calc_S(void)
{ // функция подсчета площади выкройки
S=2*pi*common->r*common->r*common->sin_A;
return S;
}

double Down::Calc_V(void)
{ // функция подсчета объема выкройки
double h=common->r-common->sin_A*common->r; // высота слоя
V=double((pi*h*h)*(common->r-h/3)); // объем сегмента
return V;
}

double Pyr::Calc_L(void){
    // функция подсчета длины выкройки
    common->r_down=qCos(common->angle)*common->r;
    L=common->r_down/qSin(common->angle);
    common->r_max=(common->r/qSin(common->angle)-common->r)*qTan(common->angle);
    return L;
}

double Pyr::Calc_S(void)
{ // функция подсчета площади выкройки
S=pi*common->r_down*L;
return S;
}

double Pyr::Calc_V(void)
{ // функция подсчета объема выкройки
V=(S*common->sin_A*L*common->cos_A)/3;
return V;
}

double Out::Calc_L(void)
{ // функция подсчета длины выкройки
L=common->r_out/common->sin_A;
return L;
}

double Out::Calc_S(void)
{ // функция подсчета площади выкройки
S=pi*common->r_out*L;
return S;
}

double Out::Calc_V(void)
{ // функция подсчета объема выкройки
V=(S*common->sin_A*L*common->cos_A)/3;
return V;
}
